#!/usr/bin/env bash

apt-get -y install cppcheck
apt-get -y install nodejs npm

npm install -g particle-cli

// this is a program to test serial connection
// notice it uses Serial1 which is the serial connection on the I/O pins, not the one using USB

int counter = 0;
int led_onboard = D7; // Instead of writing D7 over and over again, we'll write led2

void setup() {
	Serial1.begin(9600);

	pinMode(led_onboard, OUTPUT);
}

void loop() {
	Serial1.printlnf("H testing %d", ++counter);
	digitalWrite(led_onboard, HIGH);
	delay(1000);

	Serial1.printlnf("L testing %d", ++counter);
	digitalWrite(led_onboard, LOW);
	delay(1000);
}
